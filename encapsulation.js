class Employee {
    #employeeSalary; //contoh penerapan encapsulation private untuk properties
    constructor(name, salary, departemen) {
        this.employeeName = name;
        this.#employeeSalary = salary;
        this.employeeDepartemen = departemen;
    }

    #calculateSalary(){ //contoh penerapan encapsulation private untuk method
        return this.#employeeSalary + 5;
    }

    sendSalary() {
        return `kirim gaji ke ${this.employeeName} sebesar ${this.#calculateSalary()} juta`;
    }

    _insurance(){ // contoh penerapatan protected
        return "IP500";
    }
}

class Manager extends Employee{
    constructor(name, salary, departemen){
        super(name, salary, departemen)
    }

    // annualBonus(){
    //     const salaryAfterCalculate = super.#calculateSalary(); //tidak bisa mengakses karena calculateSalary adalah method private
    //     return salaryAfterCalculate * 2;
    // }
    getInsurance(){
        const insurance = super._insurance();
        return insurance;
    }
}

const zaky = new Employee("zaky", 10, "IT");
// console.log(zaky.insurance())

const suselo = new Manager("suselo", 20, "IT");
// console.log(suselo.annualBonus())
console.log(suselo.getInsurance())