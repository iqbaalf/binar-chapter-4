class Law {
    constructor(category, jumlahPasal){
        if(this.constructor === Law){
            throw new Error("ini class abstract, gak boleh di gunakana");
        }
        this.category = category;
        this.jumlahPasal = jumlahPasal;
    }

    showLaw(){
        return `undang2 tentang ${this.category} memiliki ${this.jumlahPasal} pasal`;
    }
}

class PeraturanPresiden extends Law {
    constructor(category, jumlahPasal){
        super(category, jumlahPasal)
    }
}

const kuhp = new PeraturanPresiden("kuhp", 10);
console.log(kuhp.showLaw())